import axios from 'axios'
import store from '@/store/index'
import { Message } from 'element-ui'
import router from '@/router'
import qs from 'qs'
const request = axios.create({
  // 配置选项
})

// 请求拦截器
request.interceptors.request.use((config) => {
  config.headers.Authorization = store.state?.user?.access_token
  return config
}, (error) => {
  return Promise.reject(error)
})
const redirectLogin = () => {
  router.push({
    name: 'login',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}
// const refreshTokenFn = async () => {
//   const refreshToken = store.state?.user?.refresh_token || ''
//   return await axios.create()({
//     method: 'POST',
//     url: '/front/user/refresh_token',
//     data: qs.stringify({
//       refreshtoken: refreshToken
//     })
//   })
// }
let refreshTokenLoding = false
type RefreshTokenArrayFn = () => void
const refreshTokenArray: RefreshTokenArrayFn[] = []
// 相应拦截器
request.interceptors.response.use((response) => {
  // 2xx 会进入这里
  return response
}, async (error) => {
  if (error.response) {
    // 400 401
    // 有响应但是返回的状态码非2开头
    const { status } = error.response
    if (status === 400) {
      Message.error('请求参数错误')
    } else if (status === 401) {
      const refreshToken = store.state?.user?.refresh_token || ''
      // 如果存在refresh_token
      if (refreshToken) {
        if (!refreshTokenLoding) {
          refreshTokenLoding = true
          // 尝试使用 refresh_token 获取新的 access_token
          try {
            const { data } = await axios.create()({
              method: 'POST',
              url: '/front/user/refresh_token',
              data: qs.stringify({
                refreshtoken: refreshToken
              })
            })
            if (!data.content) throw new Error('error refresh_token')
            store.commit('setUser', data.content)
            refreshTokenArray.forEach(item => item())
            return request(error.config)
            // 如果成功 则重发上次请求
          } catch (error) {
            // 如果失败 跳转至登录
            redirectLogin()
          } finally {
            refreshTokenLoding = false
          }
        } else {
          return new Promise(resolve => {
            refreshTokenArray.push(() => {
              resolve(request(error.config))
            })
          })
        }
      } else {
        // 如果没有 refresh_token 跳转至登录
        redirectLogin()
        return Promise.reject(error)
      }
      // Message.error('Token无效')
    } else if (status === 403) {
      Message.error('没有权限，请联系管理员')
    } else if (status === 404) {
      Message.error('请求资源不存在')
    } else if (status >= 500) {
      Message.error('服务端错误，请联系管理员')
    }
    console.log(status)
  } else if (error.request) {
    // 请求超时或网络断开
    Message.error('请求超时，请刷新重试')
  } else {
    // 请求时发生错误
    Message.error(`请求失败:${error.message}`)
  }
  return Promise.reject(error)
})

export default request
