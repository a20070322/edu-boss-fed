import request from '@/utils/request'
import qs from 'qs'
interface Login {
  phone: string
  password: string
}

/**
 * 用户请求模块
 */
export const login = (data: Login) => request({
  url: '/front/user/login',
  method: 'POST',
  data: qs.stringify(data)
})

/**
 * 获取登录用户信息
 */
export const getUserInfo = () => request({
  url: '/front/user/getInfo',
  method: 'GET'
})
